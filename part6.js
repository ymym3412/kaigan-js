var foo = "foo"//nodeだとこのfooはスクリプト内のスコープで定義されるため、グローバル空間には定義されない
var myObject = {foo : "I am myObject.foo"}
//グローバル空間に定義するにはこんな感じ。これでthisで触れるようになる
Object.defineProperty(global, "foo", {
    enumerable: false,
    configurable: false,
    writable: false,
    value: "foo"
})

var sayFoo = function(){
    console.log(this["foo"]);
};

myObject.sayFoo = sayFoo;

myObject.sayFoo(); //myObject内でのthisはmyobjectを指す

sayFoo();//sayFoo内でのthisはグローバルオブジェクトを指す
//console.log(global.foo)
console.log("========================================================\n\n\n")

var myObject = {
    func1: function(){
        console.log(this)//出力はmyObject
        var func2 = function(){
            console.log(this)//出力はwindow
            var func3 = function(){
                console.log(this)//output is global/window
            }();
        }();
    }
}

myObject.func1();

console.log("========================================================\n\n\n")

var foo = {
    func1: function(bar){
        bar();
        console.log(this);//output is foo
    }
}

foo.func1(function(){console.log(this)});//output is global/window

console.log("========================================================\n\n\n")
//入れ子の関数の最初の階層に参照用の変数こと作ることで入れ子内でthisを見失うことを防ぐ
var myObject = {
    myProperty: "I can see the light",
    myMethod: function(){
        var that = this;//myObjectへの参照をmyMethodに保持
        var helperFunction = function(){
            console.log(that.myProperty);//入れ子の関数の中からでもmyObjectのプロパティを参照できる
        }
    }
}

console.log("========================================================\n\n\n")
//コンストラクタ内でthisは新しく生成されるインスタンスへの参照として使用される
var Person = function(name){
    this.name = name || "john doe"; //thisは生成されるインスタンス
}

var cody = new Person("Cody Lindley");
console.log(cody.name) // Cody Lindley

var cody = Person("Cody Lindley");//new演算子を使わずにPerson()を使用
//この場合、コンストラクタ内のthisはグローバルオブジェクトを指しているため、Cody Lindleyはグローバルオブジェクトに定義される
console.log(global.name)//Cody Lindley

console.log("========================================================\n\n\n")
//prototypeメソッド内のthisは生成されるインスタンスを参照する
var Person = function(x){
    if(x){this.fullName = x;};
}

Person.prototype.whatIsFullName = function(){
    return this.fullName; //thisはPerson()コンストラクタではなく、Personから生成されたインスタンスを参照する
}

var yasu = new Person("ymym");
var shibata = new Person("shibashiba");

console.log(yasu.whatIsFullName(), shibata.whatIsFullName()); //生成されたインスタンスのfullNameが呼ばれる








