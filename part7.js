var parentFunction = function(){
    var foo = "foo";
    return function(){//無名関数を返す
        console.log(foo);
    }
}

//nestedFunctionはparentFunction()が返す無名関数の参照を保持している＝関数
nestedFunction = parentFunction();
nestedFunction(); //出力は"foo",なぜなら無名関数の出力するfooのスコープは無名関数定義時に固定される
//だからここでグローバル空間にfooを定義していも
var foo = "bar"
//nestedFunction = function(){console.log(foo)}で呼ばれるのは"foo"
nestedFunction();

console.log("\n\n\n========================================================\n\n\n")
//クロージャはスコープチェーンによって生成される
//クロージャとはスコープチェーンに存在する変数への参照を保持する関数
//クロージャの例
var countUpFromZero = function(){
    var count = 0;
    return function(){//子関数を返す
        return ++count; //変数countは親関数で定義されている
    };
}();//countUpFromZeroは呼ばれると即時実行され、無名関数を返す

console.log(countUpFromZero());//1
console.log(countUpFromZero());//2
console.log(countUpFromZero());//3
//実行されるたびに++countが実行されて値が返される


console.log("\n\n\n========================================================\n\n\n")
//インデックスを出力する関数の失敗例
var LogElementNumber = function(){
    var funcArray = [];
    var i;
    for(i = 0; i < 3; i++){
        funcArray[i] = function(){console.log(i);};//インデックスを出力する関数はfuncArrayに格納しているつもり
    }
    return funcArray;
}();

LogElementNumber[0]();//0ではなく3
LogElementNumber[1]();//1ではなく3
LogElementNumber[2]();//2ではなく3

/*
これはLogElementNumberの中に格納されている関数はiへの参照を保持する(今回は表示している)関数であるため、同じ数字が表示されてしまう
*/


console.log("\n\n\n========================================================\n\n\n")
//上記の正答例
var LogElementNumber2 = function(){
    var funcArray = [];
    var i;
    var func = function(i){
        //引数にiをしていることでこの関数スコープに新たに変数iが定義されている
        //この関数スコープのi ≠ 親関数スコープのi
        return function(){ console.log(i);};
    }

    for(i = 0; i < 3; i++){
        funcArray[i] = func(i);
    }

    return funcArray;
}();

LogElementNumber2[0]();//0
LogElementNumber2[1]();//1
LogElementNumber2[2]();//2

/*
余談:LogElementNumber2に無名関数を定義する際に即時実行したものを代入しているのはなぜ？
→即時実行しない場合返されるのは無名関数。そしてこの関数を実行すると配列が返る。
つまり即時実行しないとLogElementNumberに関数を代入したあともう１回関数を実行して返ってくる結果を変数に格納しなくてはならない。つまり２度手間
だから即時実行している
*/

console.log("\n\n\n========================================================\n\n\n")
