//関数クラスのインスタンスのprototypeプロパティはobjectである
var Person = function Person(name){
    this.name = name;
};

console.log(typeof Person.prototype);//object

//関数クラス以外のクラスのインスタンスのprototypeプロパティはobjectではない？
//全てはチェックできていない
var mary = new Person("mary");
var str = "hello";
var myArray = new Array();
console.log(typeof mary.prototype);//undefined
console.log(typeof str.prototype);//undefined
console.log(typeof myArray.prototype);//undefined


console.log("\n\n\n========================================================\n\n\n")
//__proto__は動作しない環境もある
Array.prototype.foo = "foo";
var myArray = new Array();

//__proto__はコンストラクタのprototypeへのリンクを保持している
//ただしECMAScript標準ではないことに注意
console.log(myArray.__proto__.foo);//foo
console.log(myArray.constructor.prototype.foo)//foo 上と同内容

myArray.foo = "bar"
console.log(myArray.foo)//プロトタイプチェーンをたどるのでbar
console.log(myArray.__proto__.foo)//コンストラクタのfooを直接参照

console.log("\n\n\n========================================================\n\n\n")
//プロトタイプからプロパティを継承するインスタンスは常に最新の値を取得
var Foo = function Foo(){};
Foo.prototype.x = 1;

var fooInstance = new Foo();
console.log(fooInstance.x);//1
console.log(fooInstance.y);//undefined

Foo.prototype.x = 2;
Foo.prototype.y = 3;
console.log(fooInstance.x);//2
console.log(fooInstance.y);//3

delete Foo.prototype.x;//xを削除
console.log(fooInstance.x);//undefined
console.log(fooInstance.y);3


console.log("\n\n\n========================================================\n\n\n")
//prototypeプロパティを新しいオブジェクトに置き換えると過去の・・・

var Foo = function Foo(){};
Foo.prototype.x = 1;

var fooInstance = new Foo();
console.log(fooInstance.x);//1

Foo.prototype = {x:2};
console.log(fooInstance.x);//2ではなく1を出力。fooInstance.xはオブジェクトが代入される前の古いコンストラクタのプロパティを参照しているから

console.log("\n\n\n========================================================\n\n\n")
//継承チェーンの生成
//まずはコードを書く
var Person = function(){
    this.bar = "bar";
};
Person.prototype.foo = "foo";

var Chef = function(){
    this.goo = "goo";
};
Chef.prototype = new Person();//これで継承関係が成立する
var cody = new Chef();

console.log(cody.foo);//foo
console.log(cody.bar);//bar
console.log(cody.goo);//goo

console.log(Chef.prototype.__proto__ === Person.prototype);//true

